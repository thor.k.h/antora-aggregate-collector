'use strict'

// Much of this code is copied from Antora aggregate-content.js

const EventEmitter = require('events')
const fs = require('fs-extra')
const { fs: resolvePathGlobsFs, git: resolvePathGlobsGit } = require('@djencks/content-aggregator/lib/resolve-path-globs')
const File = require('@djencks/content-aggregator/lib/file')
const git = require('isomorphic-git')
const { obj: map } = require('through2')
const ospath = require('path')
const { posix: path } = ospath
const vfs = require('vinyl-fs')
const { makeRe: makePicomatchRx } = require('picomatch')
const PICO_OPTS = { nobracket: true, noextglob: true, noquantifiers: true }//, noglobstar: true

const posixify = ospath.sep === '\\' ? (p) => p.replace(/\\/g, '/') : undefined

const {
  CONTENT_GLOB,
  FILE_MODES,
  ON_COMPONENT_DESCRIPTOR, //switch to this when #377 is published
} = require('@djencks/content-aggregator/lib/constants')

// const ON_COMPONENT_DESCRIPTOR = 'onComponentDescriptor'

const VENTILATED_CSV_RX = /\s*,\s+/
const SUPERFLUOUS_SEPARATORS_RX = /^\/+|\/+$|\/+(?=\/)/g

module.exports.register = (eventEmitter, config = {}) => {
  eventEmitter.on(ON_COMPONENT_DESCRIPTOR,
    ({ componentDescriptor, files, startPath, repo, authStatus, ref, worktreePath, origin }) => {
      // console.log('component descriptor', componentDescriptor)
      const sourceMap = componentDescriptor.sourceMap
      if (sourceMap) {
        // console.log('sourceMap', sourceMap)
        const result = sourceMap.map(({ sourcePaths, target }) =>
          collectFromPath(sourcePaths, target, repo, ref, worktreePath, componentDescriptor, files, origin))
        // console.log('onComponentDescriptor result', result)
        return Promise.all(result)
      }
    }
  )
}

function collectFromPath (sourcePaths, target, repo, ref, worktreePath, componentDescriptor, files, origin) {
  if (!sourcePaths) {
    console.log(`no sourcePaths defined for ${componentDescriptor}`)
    return
  }
  // console.log('sourcepaths', sourcePaths)
  // console.log('target', target)
  sourcePaths = Array.isArray(sourcePaths)
    ? sourcePaths.map(coerceToString).map(cleanStartPath)
    : (sourcePaths = coerceToString(sourcePaths)) && sourcePaths.split(VENTILATED_CSV_RX).map(cleanStartPath)
  // console.log('sourcepaths', sourcePaths)
  return worktreePath
    ? readFilesFromWorktree(worktreePath, sourcePaths, target, files, origin)
    : readFilesFromGitTree(repo, ref, sourcePaths, target, componentDescriptor, files, origin)
}

async function readFilesFromWorktree (worktreePath, sourcePaths, target, files, origin) {
  // console.log('sourcepaths', sourcePaths)
  // console.log('target', target)
  const targetPath = target.path
  const deindexify = target.deindexify
  const include = target.include || CONTENT_GLOB
  // const exclude = target.exclude
  sourcePaths = await resolvePathGlobsFs(worktreePath, sourcePaths)
  // console.log('resolved sourcePaths', sourcePaths)
  return Promise.all(sourcePaths.map((sourcePath) => {
    const base = path.join(worktreePath, sourcePath)
    return fs
      .stat(base)
      .catch(() => {
        throw new Error(`the source path '${sourcePath}' does not exist`)
      })
      .then((stat) => {
        // if (!stat.isDirectory()) throw new Error(`the start path '${sourcePath}' is not a directory`)
        return new Promise((resolve, reject) =>
          vfs
            .src(include, { base, cwd: base, removeBOM: false })
            .on('error', reject)
            .pipe(relativizeFiles(targetPath, deindexify))
            .pipe(collectFiles(files, resolve))
        )
      })
  }))
}

/**
 * Transforms the path of every file in the stream to a relative posix path.
 *
 * Applies a mapping function to all files in the stream so they end up with a
 * posixified path relative to the file's base instead of the filesystem root.
 * This mapper also filters out any directories (indicated by file.isNull())
 * that got caught up in the glob.
 */
function relativizeFiles (targetPath, deindexify) {
  return map((file, enc, next) => {
    if (file.isNull()) {
      next()
    } else {
      next(
        null,
        new File({
          path: computePath(file.relative, targetPath, deindexify),
          contents: file.contents,
          stat: file.stat,
          src: { abspath: file.path },
        })
      )
    }
  })
}

function computePath (relative, targetPath, deindexify) {
  // console.log(`targetPath: ${targetPath}, relative: ${relative}, posixify: ${posixify}`)
  const filepath = path.join(targetPath, posixify ? posixify(relative) : relative)
  // console.log('filepath', filepath)
  if (deindexify) {
    return `${path.dirname(filepath)}${path.extname(filepath)}`
  }
  return filepath
}

function collectFiles (files, done) {
  return map((file, enc, next) => files.push(file) && next(), () => done(files)) // prettier-ignore
}

function readFilesFromGitTree (repo, ref, sourcePaths, target, componentDescriptor, files, origin) {
  // console.log('git target', target)
  const targetPath = target.path
  const deindexify = target.deindexify
  const include = target.include || CONTENT_GLOB
  const includeRx = makePicomatchRx(include, PICO_OPTS)
  // const exclude = target.exclude
  const promises = git
    .resolveRef(Object.assign({ ref: ref.qname }, repo))
    .then((oid) => resolvePathGlobsGit(repo, oid, sourcePaths))
    .then((sourcePaths) => Promise.all(sourcePaths.map((sourcePath) =>
      getGitTree(repo, ref, sourcePath)
        .then((tree) => srcGitTree(repo, tree, targetPath, deindexify, includeRx, files)))))
  // console.log('promises', promises)
  return promises
}

// function readFilesFromGitTree (repo, ref, startPath) {
//   return getGitTree(repo, ref, startPath).then((tree) => srcGitTree(repo, tree))
// }

function getGitTree (repo, ref, startPath) {
  // console.log('ref', ref)
  return git.resolveRef(Object.assign({ ref: ref.qname }, repo))
    .then((oid) =>
      git
        .readTree(Object.assign({
          oid,
          filepath: startPath,
        }, repo))
        .catch((err) => {
          if (err.code === 'ObjectTypeError' && err.data && err.data.actual === 'blob') {
            throw new Error(`the start path '${startPath}' is not a directory`)
          }
          throw new Error(`the start path '${startPath}' does not exist`)
        })
    )
}

function srcGitTree (repo, tree, targetPath, deindexify, includeRx, files) {
  // console.log('srcGitTree', repo)
  return new Promise((resolve, reject) => {
    // const files = []
    walkGitTree(repo, tree, filterGitEntry, targetPath, deindexify, includeRx)
      .on('entry', (entry) => entryToFile(entry).then((file) => files.push(file)))
      .on('error', reject)
      // .on('end', () => resolve(() => { console.log('srcGitTree resolving', files); Promise.all(files) }))
      .on('end', () => resolve(() => Promise.all(files)))
      .start()
  })
}

function walkGitTree (repo, root, filter, targetPath, deindexify, includeRx) {
  const emitter = new EventEmitter()
  let depth = 1
  function visit (treeEntry, dirname = '') {
    depth--
    for (const entry of treeEntry.tree) {
      if (filter(entry, dirname, includeRx)) {
        const type = entry.type
        if (type === 'blob') {
          const mode = FILE_MODES[entry.mode]
          if (mode) {
            emitter.emit(
              'entry',
              Object.assign({}, repo, {
                mode,
                oid: entry.oid,
                path: computePath(path.join(dirname, entry.path), targetPath, deindexify),
              })
            )
          }
        } else if (type === 'tree') {
          depth++
          git
            .readTree(Object.assign({ oid: entry.oid }, repo))
            .then((subtree) => visit(subtree, path.join(dirname, entry.path)))
            .catch((err) => emitter.emit('error', err))
        }
      }
    }
    if (depth === 0) emitter.emit('end')
  }
  emitter.start = () => visit(root)
  return emitter
}

/**
 * Returns true if the entry should be processed or false if it should be skipped.
 */
function filterGitEntry (entry, dirname, includeRx) {
  // console.log(`entry.path ${entry.path}, dirname: ${dirname}, includeRx ${includeRx}`)
  return entry.path.charAt() !== '.' &&
    (entry.type !== 'blob' || (~entry.path.indexOf('.') &&
    includeRx.test(path.join(dirname, entry.path))))
}

function entryToFile (entry) {
  // console.log('pushing entry at ', entry.path)
  return git.readBlob(entry).then(({ blob: contents }) => {
    const stat = new fs.Stats()
    stat.mode = entry.mode
    stat.mtime = undefined
    stat.size = contents.length
    //TODO is there a more efficient way to get a UInt8Array into a File?
    return new File({ path: entry.path, contents: Buffer.from(contents.buffer), stat })
  })
}

function coerceToString (value) {
  return value == null ? '' : String(value)
}

function cleanStartPath (value) {
  return value && ~value.indexOf('/') ? value.replace(SUPERFLUOUS_SEPARATORS_RX, '') : value
}
